var model = require('../models/logon')

module.exports.doLogon = function (d) {
  return new Promise( async function(resolve, reject) {
    if (d.usuario === undefined || d.pass === undefined) {
      resolve({err: true, description: "Error en el formulario"})
    } else {
      model.doLogon(d)
      .then(function(res){
        resolve(res)
      })
    }
  })
}
