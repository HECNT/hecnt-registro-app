var express        = require('express');
var router         = express.Router();
var ctrl           = require('../controllers/logon');
var fs             = require('fs');
var verifyToken    = require('./middleware');
var jwt            = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config         = require('./config');

router.post('/', doLogon);

function doLogon(req, res) {
  var d = req.body;
  ctrl.doLogon(d)
  .then(function(result){
    if (result.data[0].solution === 2) {
      req.session.usuario = d
      res.json({err: false, session: true})
    } else {
      res.json(result)
    }
  })
}


module.exports = router;
