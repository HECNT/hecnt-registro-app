require('../services/logon');


angular.module(MODULE_NAME)
.controller('logonCtrl', ['$scope', 'LogonService', '$timeout', 'socket', function($scope, LogonService, $timeout, socket) {
  var ctrl = this;
  $scope.hola = "new life"
  $scope.logon = {}
  $scope.btnLogon = btnLogon;

  function btnLogon() {
    var d = $scope.logon;
    LogonService.doLogon(d)
    .success(function(res){
      console.log(res,'************');
      // location.reload();
    })
  }

}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
