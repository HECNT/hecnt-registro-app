// var url = helpers.getUrl();

angular.module(MODULE_NAME)
.service('LogonService', ['$http', function($http) {
  var url = "https://localhost:3000";
  var urlBase = url + '/logon';

  this.doLogon = function(d) {
    return $http.post(urlBase, d)
  }

}]);
