var conn = require('../models/main')

module.exports.doLogon = function (d) {
  return new Promise(function(resolve, reject) {
    var mysql      = require('mysql');
    var connection = mysql.createConnection({
      host     : '35.229.59.251',
      user     : d.usuario,
      password : d.pass,
      database : 'horus_app'
    });

    connection.connect();

    connection.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
      if (error) {
        resolve({ err: true, description: error })
      } else {
        resolve({ err: false, data: results })
      }
    });

    connection.end();
  })
}
